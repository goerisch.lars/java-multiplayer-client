package de.lg.client;

import de.lg.network.Message;
import de.lg.simulation.SimulationMessageHandler;
import de.lg.world.World;
import javafx.application.Application;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Lars on 02.01.2016.
 */
public class Simulation extends Application {

    // network -> simulation
    private SynchronizedQueue<Message> networkSimulationQueue;

    // simulation -> network
    private SynchronizedQueue<Message> simulationNetworkQueue;

    private Collection<Message> messageStack = new ArrayList<>();

    private SimulationMessageHandler simulationMessageHandler = null;

    private World world = null;

    public String authenticationId = null;

    private boolean isReady = false;




    private Thread networkThread = null;


    public Simulation(SynchronizedQueue<Message> networkSimulationQueue, SynchronizedQueue<Message> simulationNetworkQueue, String authenticationId) {
        this.networkSimulationQueue = networkSimulationQueue;
        this.simulationNetworkQueue = simulationNetworkQueue;

        this.authenticationId = authenticationId;
    }


    public void run() {
        // init
        //this.simulationMessageHandler = new SimulationMessageHandler(this);



        this.world = new World(12, 12);

        System.out.println("Simulation running");

        while (true) {
            if (!isReady) {

                proceedQueryMessages();
            } else {
                proceedQueryMessages();
                updatePlayerPositions();
                updateKIPositions();
            }
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        /*primaryStage.setTitle("BlockRender Test");
        primaryStage.setMinHeight(650);
        primaryStage.setMinWidth(650);
        Group root = new Group();
        this.root = root;
        Scene scene = new Scene(root, 600, 600, Color.WHITE);

        this.scene = scene;
        primaryStage.setScene(scene);
        primaryStage.show();


        addKeyListener(this.scene);

        this.label = new Label();
        this.label.setTextFill(Color.RED);
        root.getChildren().addAll(label);*/

    }
    /**
     * sort all messages in types
     */
    private void proceedQueryMessages() {
        if (!this.networkSimulationQueue.isEmpty()) {
            messageStack = this.networkSimulationQueue.getAll(true);
            for (Message message : messageStack) {
                this.simulationMessageHandler.proceedNetworkMessage(message);
            }
        }
    }

    private void updateKIPositions() {


    }

    private void updatePlayerPositions() {


    }


    public World getWorld() {
        return this.world;
    }

    public void addMessageToNetwork(Message message) {
        this.simulationNetworkQueue.add(message);
    }
}