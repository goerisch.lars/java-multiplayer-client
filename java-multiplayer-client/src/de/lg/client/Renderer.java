package de.lg.client;

import javafx.scene.Group;

/**
 * Created by Lars on 16.01.2016.
 */
public interface Renderer {

    public void render(Group root, float xOffset, float yOffset);
}
