package de.lg.client;

import de.lg.network.Message;
import de.lg.network.NetworkListener;
import de.lg.simulation.SimulationMessageHandler;
import de.lg.world.Follower;
import de.lg.world.GameObject;
import de.lg.world.World;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Camera;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Sphere;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

/**
 * Created by Lars on 16.01.2016.
 */
public class Game extends Application {


    // ########### view fields #########################################################################################

    private Scene scene = null;
    private Group root = null;
    private Game view = null;

    private final long[] frameTimes = new long[100];
    private int frameTimeIndex = 0;
    private boolean arrayFilled = false;

    private Label label = null;


    // ########### game logic fields ###################################################################################


    public static String authenticationId = UUID.randomUUID().toString();

    public static String authenticationToken = null;

    private Thread simulationThread = null;
    private Thread networkThread = null;

    public static boolean isReady = false;

    public static boolean gameReady = false;

    private boolean isRunning = true;

    private World world = null;

    public static boolean loaded = false;
    //preparation
    private SynchronizedQueue<Message> networkSimulationQueue = new SynchronizedQueue<>();
    private SynchronizedQueue<Message> simulationNetworkQueue = new SynchronizedQueue<>();

    private SimulationMessageHandler simulationMessageHandler = null;

    private boolean playmode = false;

    private int windowSizeWith = 600;
    private int windowSizeHeight = 600;

    Camera camera;
    private Group playerGroup;


    // ########################### view methods ########################################################################

    /**
     * main method to start the game
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * main function for FX
     *
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("ABGABE");
        primaryStage.setMinHeight(650);
        primaryStage.setMinWidth(650);

        playMusic();


        Group root = new Group();
        this.root = root;

        Group playerGroup = new Group();
        this.playerGroup = playerGroup;


        Scene scene = new Scene(root, windowSizeWith, windowSizeHeight, Color.WHITE);


        camera = new PerspectiveCamera(true);
        ;

        PerspectiveCamera camera = new PerspectiveCamera(true);
        camera.setTranslateZ(-1000);
        camera.setNearClip(0.1);
        camera.setFarClip(2000.0);

        camera.setFieldOfView(35);


        scene.setCamera(camera);


        Sphere sphere = new Sphere(100);
        PhongMaterial material1 = new PhongMaterial();
        material1.setDiffuseColor(Color.BLUE);
        material1.setSpecularColor(Color.LIGHTBLUE);
        material1.setSpecularPower(10.0);
        sphere.setMaterial(material1);

        root.getChildren().add(playerGroup);


        this.scene = scene;
        primaryStage.setScene(scene);
        primaryStage.show();


        addKeyListener(this.scene);

        this.label = new Label();
        label.setLayoutX(0);
        label.setLayoutY(0);

        this.label.setTextFill(Color.RED);
        root.getChildren().addAll(label);

        this.world = new World(12, 12);
        this.world.setGroup(root);

        // start network
        this.networkThread = new Thread(new NetworkListener(networkSimulationQueue, simulationNetworkQueue));
        this.networkThread.start();

        this.simulationMessageHandler = new SimulationMessageHandler(this);

        this.world.setSimulationMessageHandler(this.simulationMessageHandler);

        this.startGameLoop();
    }

    /**
     *
     */
    private void playMusic() {
//        Media media = null;
//
//        try {
//            media = new Media(new File("DigitalNative.mp3").getCanonicalPath());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        MediaPlayer mediaPlayer = null;
//
//        mediaPlayer = new MediaPlayer(media);
//
//        mediaPlayer.play();
    }

    public void init(World world) {
        this.world = world;
        this.world.getPlayer().setDisplayPosition(600, 600);
        this.view = this;


    }


    private void addKeyListener(Scene scene) {
        scene.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
            if (key.getCode() == KeyCode.UP) {
                root.setTranslateY(root.getTranslateY() + 5);
                this.world.setyOffset(this.world.getyOffset() + 5);
                this.world.updatePlayerY(-5);
                this.world.getPlayer().y = this.world.getPlayer().y - 5;

            }
            if (key.getCode() == KeyCode.DOWN) {
                root.setTranslateY(root.getTranslateY() - 5);
                this.world.setyOffset(this.world.getyOffset() - 5);
                this.world.updatePlayerY(+5);
                this.world.getPlayer().y = this.world.getPlayer().y + 5;
            }
            if (key.getCode() == KeyCode.LEFT) {
                root.setTranslateX(root.getTranslateX() - 5);
                this.world.setxOffset(this.world.getxOffset() - 5);
                this.world.updatePlayerX(-5);
                this.world.getPlayer().x = this.world.getPlayer().x + 5;
            }
            if (key.getCode() == KeyCode.RIGHT) {
                root.setTranslateX(root.getTranslateX() + 5);
                this.world.setxOffset(this.world.getxOffset() + 5);
                this.world.updatePlayerX(5);
                this.world.getPlayer().x = this.world.getPlayer().x - 5;

            }
        });
    }

    /**
     * render the view for all seeable units
     *
     * @param world
     */
    public void render(World world) {
        ArrayList<GameObject> gameObjects = world.gameObjects;
        root.getChildren().clear();
        // render only visible blocks
        for (GameObject gameObject : gameObjects) {
            gameObject.render(root, this.world.getxOffset(), this.world.getyOffset());
        }

        //this.world.getPlayer().render(root, this.world.getxOffset(), this.world.getyOffset());
        // FPS
        root.getChildren().add(label);

    }

    // ################ game logic methods #############################################################################

    /**
     * perform operations for each frame and render the view
     */
    public void startGameLoop() {
        AnimationTimer frameRateMeter = new AnimationTimer() {

            /**
             * performed with 1/60 ticks
             *
             * @param now
             */
            @Override
            public void handle(long now) {
                // ### FPS DEBUG
                long oldFrameTime = frameTimes[frameTimeIndex];
                frameTimes[frameTimeIndex] = now;
                frameTimeIndex = (frameTimeIndex + 1) % frameTimes.length;
                if (frameTimeIndex == 0) {
                    arrayFilled = true;
                }
                if (arrayFilled) {
                    long elapsedNanos = now - oldFrameTime;
                    long elapsedNanosPerFrame = elapsedNanos / frameTimes.length;
                    double frameRate = 1_000_000_000.0 / elapsedNanosPerFrame;
                    label.setText(String.format("%.3f", frameRate));
                }

//                // ## LOGIC
                if (!playmode) {
                    renderMainScreen();
                }
//
                if (!isReady) {
                    proceedQueryMessages();
                } else {
                    if (!loaded) {
                        initz();
                        root.getChildren().clear();
                        root.getChildren().add(label);
                        render(world);
                    }
                    proceedQueryMessages();


                    if (playmode) {
                        world.render();
                        world.renderKI();
                        label.setLayoutX(50);
                        label.setLayoutY(50);

                        Label s = new Label();
                        s.setText(world.getPlayer().score + "");
                        s.setMinWidth(100);
                        s.setMinHeight(100);
                        s.setTextFill(Color.WHITE);
                        s.setTranslateX(world.getPlayer().x);
                        s.setTranslateY(world.getPlayer().y);
                        root.getChildren().add(s);
                        root.getChildren().add(label);
                    }
//
                }


            }
        };
        frameRateMeter.start();
    }

    /**
     * init is a protected in use method in application, so this is named iniz
     */
    public void initz() {
        if (getWorld().getPlayer() != null) {
            // this.world.getPlayer().setDisplayPosition(600, 600);
            this.world.updateWorld(root);
            this.gameReady = true;
            this.playmode = true;

            root.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    System.out.println(event.getSceneX());
                    System.out.println(event.getSceneY());
                }
            });

            this.world.player.follower = new Follower(this.world.player);
            this.world.player.follower.setWorld(world);
            loaded = true;

        }
    }

    private void proceedQueryMessages() {
        if (!this.networkSimulationQueue.isEmpty()) {
            Collection<Message> messageStack = this.networkSimulationQueue.getAll(true);
            for (Message message : messageStack) {
                this.simulationMessageHandler.proceedNetworkMessage(message);
            }
        }
    }

    public World getWorld() {
        return this.world;
    }

    public void addMessageToNetwork(Message message) {
        this.simulationNetworkQueue.add(message);
    }


    private void renderMainScreen() {
        root.getChildren().clear();
        Rectangle state = new Rectangle();
        state.setWidth(windowSizeWith / 2);
        state.setHeight(windowSizeHeight / 5);
        state.setX((windowSizeWith / 2) / 2);
        state.setY(windowSizeHeight / 3);
        if (!gameReady) {
            state.setFill(Color.RED);
        } else {
            state.setFill(Color.GREEN);
        }
        root.getChildren().add(state);

    }

    private void updateWorld(Group root) {
        this.world.render();
    }
}
