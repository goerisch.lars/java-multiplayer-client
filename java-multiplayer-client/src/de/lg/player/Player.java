package de.lg.player;

/**
 * Created by Lars on 30.12.2015.
 */
public class Player {

    public String playerIdentifier = null;

    // spawnPosition
    public float positionX = 0.0f;
    public float positionY = 0.0f;

    public PlayerInformation playerInformation = null;
}
