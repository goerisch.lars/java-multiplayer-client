package de.lg.world;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Created by Lars on 16.01.2016.
 *
 * the main player class represents the red user on the screen
 */
public class Player extends GameObject {

    public float x;
    public float y;

    public int score = 0;

    public Follower follower = null;

    private int size = 10;

    public boolean isEnemy = false;

    public Color color  = Color.RED;

    public Player(float coordX, float coordY) {
        super(coordX, coordY);


        this.x = coordX;
        this.y = coordY;

        this.getRectangle().setX(x);
        this.getRectangle().setY(y *1); // inverse because fx inversed y
        this.getRectangle().setTranslateZ(-50);
        this.getRectangle().setWidth(25);
        this.getRectangle().setHeight(25);
        this.getRectangle().setFill(color);

        getRectangle().boundsInParentProperty().addListener(new ChangeListener<Bounds>() {
            @Override
            public void changed(ObservableValue<? extends Bounds> observable,
                                Bounds oldValue, Bounds newValue) {

                //System.out.println("Hit!");



            }
        });

    }

    public Color getColor() {
        return color;
    }

    public Player setColor(Color color) {
        this.color = color;
        this.getRectangle().setFill(color);
        return this;
    }

    // a block represent by an rectangle
    Rectangle rectangle = new Rectangle();


    /**
     * sets the player to the middle of the screen
     *
     * @param x
     * @param y
     */
    public void setDisplayPosition(float x, float y){
        this.x = (x / 2) - size;
        this.y = (y / 2) - size;
    }


    @Override
    public void render(Group root, float xOffset, float yOffset) {


        this.getRectangle().setX(x - 25);
        this.getRectangle().setY(y + 25);



        root.getChildren().add(this.getRectangle());
    }

    public float getY() {
        return y;
    }

    public Player setY(float y) {
        this.y = y;
        return this;
    }

    public float getX() {

        return x;
    }

    public Player setX(float x) {
        this.x = x;
        return this;
    }

    public float getRealY(){
        return y;
    }

    public boolean hasCollisions(Rectangle other) {
//        if (this.getRectangle().getBoundsInParent().intersects(other.getBoundsInParent())) {
//            javafx.scene.shape.Shape intersect = Rectangle.intersect(this.getRectangle(), other);
//            if (intersect.getBoundsInLocal().getWidth() != -1) {
//                // Collision handling here
//                System.out.println("Collision occured");
//                return true;
//            }
//        }
//        return false;

       return false;
    }


    public float getTranslatetCoordY(){
        return this.y * -1;
    }

    public float getTranslatetCoordX(){
        return this.x * -1;
    }


    public Rectangle getRectangle() {
        return rectangle;
    }


    @Override
    public String toString() {
        return this.getCoordX() + "#"  + this.getCoordY() + "a";
    }
}
