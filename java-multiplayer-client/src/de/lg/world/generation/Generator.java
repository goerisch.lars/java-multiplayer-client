package de.lg.world.generation;

import de.lg.world.*;
import de.lg.world.facades.Chunks;
import javafx.scene.paint.Color;

import java.util.Random;

/**
 * Created by Lars on 30.12.2015.
 *
 *
 */
public class Generator {

    private double[][] noiseArray = null;


    public Generator(double[][] noiseArray, float coordX, float coordY, int howMuchChunks) {
        this.noiseArray = noiseArray;
    }

    /**
     * generates chunks from an noiseArray
     *
     * @param noiseArray
     * @param chunkSize
     * @return
     */

    /**
     * @param chunksLength
     * @return
     */
    public ChunkHolder generateChunkHolder(float startX, float startY, int chunksLength, Chunk.ChunkSize chunkSize) {

        // noise size
        int size = chunksLength * chunkSize.getValue();


        double[][] noisePattern = generateNoisePattern(size, 100, 0.5, 5000);

        // blocks
        Block[] blocks = generateBlocks(startX, startY, noisePattern);

        // chunks
        Chunk[] chunks = Chunks.generateChunksFromBlocks(blocks, chunkSize, chunksLength);
        // holder

        ChunkHolder chunkHolder = new ChunkHolder((int)startX, (int)startY, chunks);
        return chunkHolder;


    }

    public Sector generateSector(float startX, float startY, double[][] noisePattern) {

        // blocks
        Block[] blocks = generateBlocks(startX, startY, noisePattern);

        return new Sector((int)startX,(int)startY,blocks);


    }

    private Block[] generateBlocks(float startX, float startY, double[][] noisePattern) {

        Block[] blocks = new Block[noiseArray.length * noiseArray[0].length];

        int stepSizeX = noiseArray.length;
        int stepSizeY = noiseArray[0].length;

        float coordXTmp = startX;
        float coordYTmp = startY;

        int arrayStepper = 0;

        Random random = new Random();
        random.setSeed((long)(startX * startY));

        Color color = new Color(random.nextFloat() * 1, random.nextFloat() * 1, random.nextFloat() * 1, 1);
        // top to down
        for (double[] aNoisePattern : noisePattern) {
            // left to right
            for (double anANoisePattern : aNoisePattern) {
                blocks[arrayStepper] = new Block(coordXTmp, coordYTmp, anANoisePattern);
                blocks[arrayStepper].setcolor(new Color(random.nextFloat() * 1, random.nextFloat() * 1,  random.nextFloat() * 1, 1));
                coordXTmp = coordXTmp + Sectors.BLOCK_SIZE;
                arrayStepper++;
            }
            coordYTmp  = coordYTmp - Sectors.BLOCK_SIZE;
            coordXTmp = startX;
        }

        return blocks;
    }

    private double[][] generateNoisePattern(int size, int largestFeature, double persistence, int seed) {

        SimplexNoise simplexNoise = new SimplexNoise(largestFeature, persistence, seed);

        double xStart = 0;
        double XEnd = 500;
        double yStart = 0;
        double yEnd = 500;

        int xResolution = size;
        int yResolution = size;

        double[][] result = new double[xResolution][yResolution];

        for (int i = 0; i < xResolution; i++) {
            for (int j = 0; j < yResolution; j++) {
                int x = (int) (xStart + i * ((XEnd - xStart) / xResolution));
                int y = (int) (yStart + j * ((yEnd - yStart) / yResolution));
                result[i][j] = 0.5 * (1 + simplexNoise.getNoise(x, y));
            }
        }

        return result;
    }

}
