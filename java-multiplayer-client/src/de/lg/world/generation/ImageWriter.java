package de.lg.world.generation;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 *  usefull vor debugging the simplex values
 */
public class ImageWriter {

    public static void greyWriteImage(String name, double[][] data) {
        //this takes and array of doubles between 0 and 1 and generates a grey scale image from them

        BufferedImage image = new BufferedImage(data.length, data[0].length, BufferedImage.TYPE_INT_RGB);

        for (int y = 0; y < data[0].length; y++) {
            for (int x = 0; x < data.length; x++) {
                if (data[x][y] >= 0.7f) {
                    data[x][y] = 1;
                }
                if (data[x][y] < 0.7f) {
                    data[x][y] = 0;
                }

                Color col = new Color((float) data[x][y], (float) data[x][y], (float) data[x][y]);
                image.setRGB(x, y, col.getRGB());
            }
        }

        try {
            // retrieve image
            File outputfile = new File("C:\\worldImages\\" + name + ".png");
            outputfile.createNewFile();

            ImageIO.write(image, "png", outputfile);
        } catch (IOException e) {
           e.printStackTrace();
        }
    }


}