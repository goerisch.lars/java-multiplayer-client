package de.lg.world;


import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Created by Lars on 30.12.2015.
 * <p>
 * a block represents an square on the map with 1 m x 1m length
 * the block is starting at the left top point
 */
public class Block extends GameObject {

    String texturePath = null;

    boolean blocked = false;

    double value;

    // a block represent by an rectangle
    Rectangle rectangle = new Rectangle();

    private final float x;
    private final float y;
    private final float size;

    private Color color = new Color(Math.random() * 1, Math.random() * 1, Math.random() * 1, 1);

    public void setcolor(Color color) {
        this.color = color;

    }

    /**
     * @param coordX
     * @param coordY
     * @param value  double value from noisePattern
     */
    public Block(float coordX, float coordY, double value) {
        super(coordX, coordY);
        this.value = value;
        this.x = coordX;
        this.y = coordY;
        this.size = Sectors.BLOCK_SIZE;

        this.getRectangle().setX(x);
        this.getRectangle().setY(y *1); // inverse because fx inversed y
        this.getRectangle().setWidth(size);
        this.getRectangle().setHeight(size);

        this.getRectangle().setStroke(Color.SILVER);
        if (value == 10f) {
            this.getRectangle().setTranslateZ(3);
            this.getRectangle().setFill(Color.YELLOWGREEN);
        }
        if (x < -550) {
            this.getRectangle().setTranslateZ(3);
            this.getRectangle().setFill(Color.AQUA);
        }

        if (x > 500) {
            this.getRectangle().setTranslateZ(3);
            this.getRectangle().setFill(Color.AQUA);
        }
    }


    public float renderCoordX(float offSett) {
        return x + offSett;
    }

    public float renderCoordY(float offSett) {
        return y + offSett;
    }

    @Override
    public void render(Group root, float xOffset, float yOffset) {
        this.getRectangle().setStroke(color);
        root.getChildren().add(this.getRectangle());
    }

    public boolean hasCollisions(Rectangle other) {
        if (this.getRectangle().getBoundsInParent().intersects(other.getBoundsInParent())) {
            javafx.scene.shape.Shape intersect = Rectangle.intersect(this.getRectangle(), other);
            if (intersect.getBoundsInLocal().getWidth() != -1) {
                // Collision handling here
                System.out.println("Collision occured");
                return true;
            }
        }
        return false;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }


    @Override
    public String toString() {
        return this.getCoordX() + "#"  + this.getCoordY() + "a";
    }
}
