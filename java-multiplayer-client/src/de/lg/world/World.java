package de.lg.world;

import de.lg.network.messages.KiMessage;
import de.lg.network.messages.MovementMessage;
import de.lg.network.messages.WorldSectorMessage;
import de.lg.simulation.SimulationMessageHandler;
import de.lg.world.generation.SectorGenerator;
import javafx.scene.Group;
import javafx.scene.paint.Color;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

/**
 * Created by Lars on 16.01.2016.
 * <p>
 * World class represents the whole map with all functions for players and items
 */
public class World {

    private float VIEW_SIZE = 12;
    private float VIEW_BUFFER = 1;

    // represents the world scalingfactor based on server world
    private int scalingFactor = 50;

    private float BLOCK_SIZE = scalingFactor;

    // sector size in BLOCKS
    private int SECTOR_SIZE = 12;

    private final float MOVEMENT_SPEED = 5;


    public final ArrayList<GameObject> gameObjects = new ArrayList<>();

    public final ArrayList<Item> items = new ArrayList<>();

    public final ArrayList<GameObject> obstracles = new ArrayList<>();

    public final HashMap<String, Sector> sectors = new HashMap<>();

    public HashMap<String, Player> players = new HashMap<>();

    private Group root;

    public Player player;

    private float xOffset = 0;
    private float yOffset = 0;

    private SimulationMessageHandler simulationMessageHandler;


    public World(int with, int height) {
        this.init(with, height);
    }

    public void setWorldContext() {

    }

    public void init(int with, int height) {

        this.setPlayer(new Player(0, 0));

        // TODO: set world offset initial by player pos
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void movePlayer(Vector vector) {

        // move player to angle and with movementspeed

    }

    public void setGroup(Group root) {
        this.root = root;
    }

    /**
     * collect all objects visible on the screen
     * huge performance upgrade if the world holds a lot of objects
     *
     * @return
     */
    public ArrayList<GameObject> getViewable() {

        float xOFF = this.player.getCoordX() * -1;
        float yOFF = this.player.getCoordY();

        ArrayList<GameObject> viewableObjects = new ArrayList<>();

        // only objects in viewPort
        for (GameObject gameObject : this.gameObjects) {
            if (gameObject instanceof Player) {
                viewableObjects.add(gameObject);
                continue;
            }
            if (gameObject == null) {
                continue;
            }
            if (gameObject.getCoordX() >= xOFF - 400 && gameObject.getCoordX() <= xOFF + 400) {
                if (gameObject.getCoordY() <= yOFF + 400 && gameObject.getCoordY() >= yOFF - 400) {
                    viewableObjects.add(gameObject);
                }
            }
        }

        viewableObjects.addAll(items);
        viewableObjects.addAll(players.values());
        return viewableObjects;
    }

    public float getxOffset() {
        return xOffset;
    }

    public World setxOffset(float xOffset) {
        this.xOffset = xOffset;
        return this;
    }

    public float getyOffset() {
        return yOffset;
    }

    public World setyOffset(float yOffset) {
        this.yOffset = yOffset;
        return this;
    }

    public ArrayList<GameObject> getBlocks() {
        return gameObjects;
    }

    public Player getPlayer() {
        return player;
    }


    /**
     * updates the static environment
     *
     * @param root
     */
    public void updateWorld(Group root) {
        if (this.getPlayer() != null) {
            validateWorldSectors(root, this.getPlayer(), true);
            cleanupWorldSectors();
        }
    }

    /**
     * remove worldSectors that are not necessary anymore
     * to clean up memory
     */
    private void cleanupWorldSectors() {


    }

    /**
     * checks if the sector around the player are loaded or if need to hold more
     *
     * @param root
     * @param player
     * @param loadSector - define if sector loaded from the server if not loaded
     */
    private boolean validateWorldSectors(Group root, Player player, boolean loadSector) {
        float[][] sectorCoords = Sectors.getSectorCoordsAround(player.getCoordX(), player.getCoordY());

        boolean changes = false;
        for (float[] sectorCoord : sectorCoords) {
            boolean isLoaded = this.sectorIsLoaded(sectorCoord[0], sectorCoord[1]);
            if (!isLoaded) {
                if (loadSector) {
                    changes = true;
                    this.generateSector(sectorCoord[0], sectorCoord[1]);
                }
            }
        }
        return true;
    }


    public Sector generateSector(float x, float y) {

        SectorGenerator sectorGenerator = new SectorGenerator();
        String id = Sectors.generateSectorNumberString(x, y);
        Sector sector = sectorGenerator.generateSector(x, y);
        this.sectors.put(id, sector);

        Block[] block = sector.getBlocks();
        for (Block block1 : block) {
            if (block1 != null) {
                this.gameObjects.add(block1);
            }

        }
        return sector;
    }

    public boolean sectorIsLoaded(float x, float y) {
        Sector sector = this.sectors.get(Sectors.generateSectorNumberString(x, y));
        if (sector == null) {
            return false;
        } else {
            return true;
        }
    }

    public void updatePlayerX(float moveDistance) {
        MovementMessage movementMessage = new MovementMessage();
        movementMessage.setMoveToX(this.player.getCoordX() + moveDistance);
        movementMessage.setMoveToY(this.player.getCoordY());
        this.getSimulationMessageHandler().getGame().addMessageToNetwork(movementMessage);
        this.player.setCoordX(this.player.getCoordX() + moveDistance);
    }

    private void checkItems() {

        for (Item item : items) {
            Rectangle r = new Rectangle((int) item.getCoordX(), (int) item.getCoordY(), 25, 25);
            Rectangle p = new Rectangle((int) this.getPlayer().follower.getCoordX(), (int) this.getPlayer().follower.getCoordY(), 25, 25);


            if (r.getBounds().intersects(p)) {
                System.err.println(r.getX() + " " + r.getY() + "|" + p.getX() + " " + p.getY());
                WorldSectorMessage worldSectorMessage = new WorldSectorMessage();
                worldSectorMessage.setAuthenticationToken(this.getSimulationMessageHandler().getGame().authenticationToken);
                worldSectorMessage.type = "POINTS";
                worldSectorMessage.setData(item.toString());
                this.getSimulationMessageHandler().getGame().addMessageToNetwork(worldSectorMessage);
                System.err.println("ITEM");
            }
        }

    }

    public void updatePlayerY(float moveDistance) {
        MovementMessage movementMessage = new MovementMessage();
        movementMessage.setMoveToX(this.player.getCoordX());
        movementMessage.setMoveToY(this.player.getCoordY() + moveDistance);
        this.getSimulationMessageHandler().getGame().addMessageToNetwork(movementMessage);
        this.player.setCoordY(this.player.getCoordY() + moveDistance);
    }


    public void setSimulationMessageHandler(SimulationMessageHandler simulationMessageHandler) {
        this.simulationMessageHandler = simulationMessageHandler;
    }

    public SimulationMessageHandler getSimulationMessageHandler() {
        return simulationMessageHandler;
    }

    public Sector getSector(int x, int y) {
        String id = Sectors.generateSectorNumberString(x, y);
        return this.sectors.get(id);
    }


    /**
     * render the world
     */
    public void render() {
        //System.err.println(player.getCoordX() + " " + player.getCoordY());

        if (this.gameObjects.size() > 5000) {

            this.gameObjects.clear();
            this.sectors.clear();
            System.err.println("clear memory");
            this.gameObjects.addAll(obstracles);
        }


        root.getChildren().clear();
        float[][] sectorCoords = Sectors.getSectorCoordsAround(player.getCoordX(), player.getCoordY());
        for (float[] sectorCoord : sectorCoords) {
            boolean isLoaded = this.sectorIsLoaded(sectorCoord[0], sectorCoord[1]);
            if (!isLoaded) {
                this.generateSector(sectorCoord[0], sectorCoord[1]);
            }
        }


        for (GameObject gameObject : this.getViewable()) {
            gameObject.render(root, 0, 0);
        }
        this.player.render(root, 0, 0);
        this.player.follower.render(root, 0, 0);

    }

    public void updatePlayerMoveMent(MovementMessage movementMessage) {
        if (!movementMessage.isMoveIsValid()) {
            this.getPlayer().setCoordX(0);
            this.getPlayer().setCoordY(0);
            this.getPlayer().x = 0;
            this.getPlayer().y = 0;
            this.root.setTranslateX(0);
            this.root.setTranslateY(0);
        }


    }

    public void addEnemyPlayer(Player player, String token) {
        this.players.put(token, player);
    }

    public void updateEnemyMovement(MovementMessage movementMessage, String token) {
        Player player = this.players.get(token);
        if (player != null) {
            player.setCoordX(movementMessage.getMoveToX() * -1);
            player.setCoordY(movementMessage.getMoveToY());
            player.setX(movementMessage.getMoveToX() * -1);
            player.setY(movementMessage.getMoveToY());
        } else {
            Player player2 = new Player(movementMessage.getMoveToX() * -1, movementMessage.getMoveToY());
            player2.isEnemy = true;
            player2.setX(movementMessage.getMoveToX() * -1);
            player2.setY(movementMessage.getMoveToY());
            addEnemyPlayer(player2, token);
        }
    }

    public void updateKiMovement(KiMessage kiMessage, String token) {
        Player player = this.players.get(token);
        if (player != null) {
            player.setCoordX(kiMessage.getMoveToX());
            player.setCoordY(kiMessage.getMoveToY());
            player.isEnemy = true;
            player.setX(kiMessage.getMoveToX());
            player.setY(kiMessage.getMoveToY());
        } else {
            player = new Player(kiMessage.getMoveToX(), kiMessage.getMoveToY());
            player.isEnemy = true;
            player.setX(kiMessage.getMoveToX());
            player.setY(kiMessage.getMoveToY());
            player.setColor(Color.GOLD);
            addEnemyPlayer(player, token);
        }
    }


    public void renderKI() {
        //System.out.println(this.player.getX() + " " +this.player.getY());

        float px = this.player.getX();
        float py = this.player.getY();

        float fx = this.player.follower.getX();
        float fy = this.player.follower.getY();


        if (this.player.follower.isAttack()) {
            moveToTarget(this.player.follower);
        } else {
            for (Item item : items) {
                boolean range = checkItemRange(item, this.player.follower);
                if (range) {
                    this.player.follower.setAttack(true);
                    this.player.follower.targetX = item.getCoordX();
                    this.player.follower.targetY = item.getCoordY();
                    break;
                }
            }

            float tmpX = fx;
            float tmpY = fy;
            if (px > 0) {
                if (fx > px) {
                    if ((fx - px) > (50 + (int) (Math.random() * ((320 - 100) + 1)))) {
                        tmpX = fx - (0 + (int) (Math.random() * ((8 - 0) + 1)));
                    }
                } else {
                    if ((px - fx) > (100 + (int) (Math.random() * ((230 - 100) + 1)))) {
                        tmpX = fx + (0 + (int) (Math.random() * ((8 - 0) + 1)));
                    }
                }

            } else {
                if (fx < px) {
                    if ((px - fx) > (40 + (int) (Math.random() * ((390 - 100) + 1)))) {
                        tmpX = fx + (1 + (int) (Math.random() * ((4 - 1) + 1)));
                    }
                } else {
                    if ((fx - px) > (100 + (int) (Math.random() * ((350 - 100) + 1)))) {
                        tmpX = fx - (-1 + (int) (Math.random() * ((8 - -1) + 1)));
                    }
                }
            }

            if (py > 0) {
                if (fy > py) {
                    if ((fy - py) > (100 + (int) (Math.random() * ((230 - 100) + 1)))) {
                        tmpY = fy - (0 + (int) (Math.random() * ((5 - 0) + 1)));
                    }
                } else {
                    if ((py - fy) > (100 + (int) (Math.random() * ((520 - -1) + 1)))) {
                        tmpY = fy + (1 + (int) (Math.random() * ((4 - 1) + 1)));
                    }
                }

            } else {
                if (fy < py) {
                    if ((py - fy) > (100 + (int) (Math.random() * ((560 - 100) + 1)))) {
                        tmpY = fy + (2 + (int) (Math.random() * ((4 - 2) + 1)));
                    }
                } else {
                    if ((fy - py) > (100 + (int) (Math.random() * ((340 - 100) + 1)))) {
                        tmpY = fy - (1 + (int) (Math.random() * ((5 - 1) + 1)));
                    }
                }
            }

            this.player.follower.setX(tmpX);
            this.player.follower.setY(tmpY);
            if (!(fx == tmpX && fy == tmpY)) {
                checkItems();

                KiMessage message = new KiMessage();
                message.setMoveToX(tmpX);
                message.setMoveToY(tmpY);
                message.setKiId(this.player.follower.getKiId());
                this.getSimulationMessageHandler().getGame().addMessageToNetwork(message);
            }


        }

    }

    private void moveToTarget(Follower follower) {

        float fx = follower.getX();
        float fy = follower.getY();

        float px = follower.targetX;
        float py = follower.targetY;


        float moveDistance = (1 + (int) (Math.random() * ((5 - 1) + 1)));

        float tmpX = fx;
        float tmpY = fy;
        if (px > 0) {
            if (fx > px) {
                if ((fx - px) > 1) {
                    tmpX = fx - moveDistance;
                }
            } else {
                if ((px - fx) > 1) {
                    tmpX = fx + moveDistance;
                }
            }

        } else {
            if (fx < px) {
                if ((px - fx) > 1) {
                    tmpX = fx + moveDistance;
                }
            } else {
                if ((fx - px) > 1) {
                    tmpX = fx - moveDistance;
                }
            }
        }

        if (py > 0) {
            if (fy > py) {
                if ((fy - py) > 1) {
                    tmpY = fy - moveDistance;
                }
            } else {
                if ((py - fy) > 1) {
                    tmpY = fy + moveDistance;
                }
            }

        } else {
            if (fy < py) {
                if ((py - fy) > 1) {
                    tmpY = fy + moveDistance;
                }
            } else {
                if ((fy - py) > 1) {
                    tmpY = fy - moveDistance;
                }
            }
        }

        this.player.follower.setX(tmpX);
        this.player.follower.setY(tmpY);
        if (!(fx == tmpX && fy == tmpY)) {
            checkItems();

            KiMessage message = new KiMessage();
            message.setMoveToX(tmpX);
            message.setMoveToY(tmpY);
            message.setKiId(this.player.follower.getKiId());
            this.getSimulationMessageHandler().getGame().addMessageToNetwork(message);
        } else {
            this.player.follower.setAttack(false);
        }

    }


    public boolean checkItemRange(Item item, Player player) {

        float px = item.getCoordX();
        float py = item.getCoordY();

        float fx = this.player.getX();
        float fy = this.player.getY();

        double distance = Math.sqrt(Math.pow((px - fx), 2) + Math.pow((py - fy), 2));

        float tmpX = fx;
        float tmpY = fy;


        if (distance < 300) {
            return true;
        }


        return false;
    }
}
