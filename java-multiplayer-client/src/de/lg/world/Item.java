package de.lg.world;


import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.io.Serializable;

/**
 *  an item, user can collect them to get points
 */
public class Item extends GameObject implements Serializable {


    int points = 1;

    public Item(float coordX, float coordY) {
        super(coordX, coordY);
    }


    @Override
    public void render(Group root, float xOffset, float yOffset) {
        Circle circle = new Circle();
        circle.setCenterX(getCoordX() -25);
        circle.setCenterY(getCoordY() +25);
        circle.setRadius(25);
        circle.setFill(Color.YELLOW);



        root.getChildren().addAll(circle);
    }

    @Override
    public String toString() {
        return this.getCoordX() + "#"  + this.getCoordY() + "a";
    }
}
