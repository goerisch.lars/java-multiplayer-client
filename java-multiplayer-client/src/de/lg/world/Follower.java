package de.lg.world;


import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.UUID;

/**
 * Created by Lars on 23.01.2016.
 * <p>
 * the ghost follower :) only visible for the user
 */
public class Follower extends Player {

    public String kiId = UUID.randomUUID().toString();
    Player player = null;

    float targetX = 0;
    float targetY = 0;

    boolean attack = false;
    private World world;

    public Color color = Color.GOLD;

    public Follower(Player player) {
        super(player.x + 100, player.y + 100);
        this.x = player.x + 100;
        this.y = player.y + 100;
        this.player = player;


        this.setColor(color);

    }

    public boolean isAttack() {
        return attack;
    }

    public Follower setAttack(boolean attack) {
        this.attack = attack;
        return this;
    }

    public Player getPlayer() {
        return player;
    }

    public Follower setPlayer(Player player) {
        this.player = player;
        return this;
    }

    public float getTargetX() {
        return targetX;
    }

    public Follower setTargetX(int targetX) {
        this.targetX = targetX;
        return this;
    }

    public float getTargetY() {
        return targetY;
    }

    public Follower setTargetY(int targetY) {
        this.targetY = targetY;
        return this;
    }

    public Follower(float coordX, float coordY) {
        super(coordX, coordY);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public Rectangle getRectangle() {
        return super.getRectangle();
    }

    @Override
    public boolean hasCollisions(Rectangle other) {
        return super.hasCollisions(other);
    }

    @Override
    public float getTranslatetCoordX() {
        return super.getTranslatetCoordX();
    }

    @Override
    public float getTranslatetCoordY() {
        return super.getTranslatetCoordY();
    }

    @Override
    public float getRealY() {
        return super.getRealY();
    }

    @Override
    public float getX() {
        return super.getX();
    }

    @Override
    public Player setX(float x) {
        super.setCoordX(x);
        return super.setX(x);
    }

    @Override
    public Player setY(float y) {
        super.setCoordY(y);
        return super.setY(y);
    }

    @Override
    public void render(Group root, float xOffset, float yOffset) {
        this.getRectangle().setFill(Color.DARKGOLDENROD);
        this.getRectangle().setX(x - 25);
        this.getRectangle().setY(y + 25);
        root.getChildren().add(this.getRectangle());
    }

    @Override
    public void setDisplayPosition(float x, float y) {
        super.setDisplayPosition(x, y);
    }

    @Override
    public float getY() {
        return super.getY();
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public String getKiId() {
        return kiId;
    }

    public Follower setKiId(String kiId) {
        this.kiId = kiId;
        return this;
    }
}
