package de.lg.world;

import java.awt.color.ColorSpace;
import java.io.Serializable;

/**
 * Created by Lars on 22.01.2016.
 */
public class ColorImp extends java.awt.Color implements Serializable{


    public ColorImp(int r, int g, int b) {
        super(r, g, b);
    }

    public ColorImp(int rgb) {
        super(rgb);
    }

    public ColorImp(int r, int g, int b, int a) {
        super(r, g, b, a);
    }

    public ColorImp(int rgba, boolean hasalpha) {
        super(rgba, hasalpha);
    }

    public ColorImp(float r, float g, float b, float a) {
        super(r, g, b, a);
    }

    public ColorImp(ColorSpace cspace, float[] components, float alpha) {
        super(cspace, components, alpha);
    }

    public ColorImp(float r, float g, float b) {
        super(r, g, b);
    }
}

