package de.lg.world;

import de.lg.client.Renderer;

/**
 * Created by Lars on 16.01.2016.
 */
public abstract class GameObject extends CoordinateFloat implements Renderer {

    public GameObject(float coordX, float coordY) {
        super(coordX, coordY);
    }

}
