package de.lg.simulation;

import de.lg.network.Message;
import de.lg.network.messages.AuthenticationMessage;

/**
 * Created by Lars on 19.01.2016.
 */
public class SimulationAuthenticationMessageHandler implements SimulationMessageHandlerI {

    private SimulationMessageHandler simulationMessageHandler = null;


    public SimulationAuthenticationMessageHandler(SimulationMessageHandler simulationMessageHandler) {
        this.simulationMessageHandler = simulationMessageHandler;
    }

    @Override
    public Class getHandledClass() {
        return AuthenticationMessage.class;
    }

    @Override
    public void announce() {

    }

    @Override
    public void handleMessage(Message message) {
        // check if player is online if not create on and add them to world
        AuthenticationMessage authenticationMessage = (AuthenticationMessage) message;
        this.simulationMessageHandler.getGame().authenticationToken = authenticationMessage.getAuthenticationToken();
        System.out.println("get token" + authenticationMessage.getAuthenticationToken());
    }
}
