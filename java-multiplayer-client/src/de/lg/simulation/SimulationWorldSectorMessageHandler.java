package de.lg.simulation;

import de.lg.network.Message;
import de.lg.network.messages.WorldSectorMessage;
import de.lg.world.Item;

/**
 * Created by Lars on 22.01.2016.
 */
public class SimulationWorldSectorMessageHandler implements SimulationMessageHandlerI {
    private SimulationMessageHandler networkMessageHandler = null;

    public SimulationWorldSectorMessageHandler(SimulationMessageHandler networkMessageHandler) {
        this.networkMessageHandler = networkMessageHandler;
    }

    @Override
    public void handleMessage(Message message) {
        WorldSectorMessage worldSectorMessage = (WorldSectorMessage) message;
//
//
//        String[] parts = worldSectorMessage.getData().split("a");
//        this.networkMessageHandler.getGame().getWorld().ObstracleFactory(parts);
//        System.err.println(worldSectorMessage.getData());

        if (worldSectorMessage.getAuthenticationToken().equals("SERVERMESSAGE") && worldSectorMessage.type.equals("ITEM")) {
            String[] parts = ((WorldSectorMessage) message).data.split("a");
            for (String string : parts) {
                String[] obstracleCoords = string.split("#");
                try {
                    Item item = new Item(Float.parseFloat(obstracleCoords[0]), Float.parseFloat(obstracleCoords[1]));
                    for(Item items : this.networkMessageHandler.getGame().getWorld().items){
                        if(items.toString().equals(item.toString())){
                            return;
                        }
                    }
                    this.networkMessageHandler.getGame().getWorld().items.add(item);
                } catch (Exception e) {
                    System.err.println(obstracleCoords);
                }

            }
        }

        if (worldSectorMessage.getAuthenticationToken().equals("SERVERMESSAGE") && worldSectorMessage.type.equals("DESTROY")) {
            String[] parts = ((WorldSectorMessage) message).data.split("a");
            for (String string : parts) {
                String[] obstracleCoords = string.split("#");

                Item item = new Item(Float.parseFloat(obstracleCoords[0]), Float.parseFloat(obstracleCoords[1]));
                this.networkMessageHandler.getGame().getWorld().items.remove(item);

                for (int x = 0; x < this.networkMessageHandler.getGame().getWorld().items.size(); x++) {
                    if (item.toString().equals(this.networkMessageHandler.getGame().getWorld().items.get(x).toString())) {
                        this.networkMessageHandler.getGame().getWorld().items.remove(x);
                        this.networkMessageHandler.getGame().getWorld().gameObjects.clear();
                        this.networkMessageHandler.getGame().getWorld().sectors.clear();
                    }

                }

                System.err.println("DESTROY");
            }
        }

        if (worldSectorMessage.type.equals("SCORE")) {

            int score = this.networkMessageHandler.getGame().getWorld().player.score;
            this.networkMessageHandler.getGame().getWorld().player.score = score + 1;

            System.err.println("SCORE");

        }

    }

    @Override
    public void announce() {

    }

    @Override
    public Class getHandledClass() {
        return WorldSectorMessage.class;
    }
}
