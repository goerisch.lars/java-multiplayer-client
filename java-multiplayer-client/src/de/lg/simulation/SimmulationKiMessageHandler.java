package de.lg.simulation;

import de.lg.network.Message;
import de.lg.network.messages.KiMessage;

/**
 * Created by Lars on 23.01.2016.
 */
public class SimmulationKiMessageHandler implements SimulationMessageHandlerI {

    private SimulationMessageHandler simulationMessageHandler = null;

    public SimmulationKiMessageHandler(SimulationMessageHandler simulationMessageHandler) {
        this.simulationMessageHandler = simulationMessageHandler;

    }

    @Override
    public void handleMessage(Message message) {
        KiMessage message1 = (KiMessage) message;
        if(this.simulationMessageHandler.getGame().getWorld().getPlayer() != null && this.simulationMessageHandler.getGame().getWorld().getPlayer().follower != null){
            if(!message1.getKiId().equals(this.simulationMessageHandler.getGame().getWorld().getPlayer().follower.getKiId())){
                this.simulationMessageHandler.getGame().getWorld().updateKiMovement(message1, message1.getKiId());
            }
        }
    }

    @Override
    public void announce() {

    }

    @Override
    public Class getHandledClass() {
        return KiMessage.class;
    }
}
