package de.lg.simulation;


import de.lg.client.Game;
import de.lg.client.SynchronizedQueue;
import de.lg.network.Message;

import java.util.HashMap;

/**
 * Created by Lars on 19.01.2016.
 *
 * main handler for Simulation task messages
 */
public class SimulationMessageHandler {

    public SynchronizedQueue<Message> simulationNetworkQueue = null;
    public HashMap<String, SimulationMessageHandlerI> messageHandlers = new HashMap<>();

    private Game game = null;

    public SimulationMessageHandler(Game game) {
        this.game = game;
        initMessageHandlers();
    }

    public void initMessageHandlers() {

        SimulationAuthenticationMessageHandler simulationAuthenticationMessageHandler = new SimulationAuthenticationMessageHandler(this);
        registerHandler(simulationAuthenticationMessageHandler);

        SimulationPlayerMessageHandler simulationPlayerMessageHandler = new SimulationPlayerMessageHandler(this);
        registerHandler(simulationPlayerMessageHandler);

        SimulationWorldSectorMessageHandler worldSectorMessageHandler = new SimulationWorldSectorMessageHandler(this);
        registerHandler(worldSectorMessageHandler);

        SimulationMovementMessageHandler simulationMovementMessageHandler = new SimulationMovementMessageHandler(this);
        registerHandler(simulationMovementMessageHandler);

        SimmulationKiMessageHandler kiMessageHandler = new SimmulationKiMessageHandler(this);
        registerHandler(kiMessageHandler);

    }

    /**
     * register an handler
     *
     * @param handler
     */
    private void registerHandler(SimulationMessageHandlerI handler) {
        messageHandlers.put(handler.getClass().toString(), handler);
    }


    public void proceedNetworkMessage(Object object) {

        for (HashMap.Entry<String, SimulationMessageHandlerI> entry : messageHandlers.entrySet()) {
            Class handledClass = entry.getValue().getHandledClass();

            if (object.getClass().equals(handledClass)) {
                entry.getValue().handleMessage((Message) object);
            }
        }
        try {
            // throw new Exception("NetworkMessageHandler object of type NetworkMessage expected got " + object.getClass());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Game getGame() {
        return this.game;
    }
}
