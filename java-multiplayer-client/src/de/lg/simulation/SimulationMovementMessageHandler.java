package de.lg.simulation;

import de.lg.network.Message;
import de.lg.network.messages.MovementMessage;

/**
 * Created by Lars on 23.01.2016.
 */
public class SimulationMovementMessageHandler implements SimulationMessageHandlerI {


    private SimulationMessageHandler simulationMessageHandler = null;

    public SimulationMovementMessageHandler(SimulationMessageHandler simulationMessageHandler) {
        this.simulationMessageHandler = simulationMessageHandler;
    }

    @Override
    public void handleMessage(Message message) {
        MovementMessage movementMessage = (MovementMessage) message;
        if (!movementMessage.getPlayerId().equals(this.simulationMessageHandler.getGame().authenticationToken)) {

            this.simulationMessageHandler.getGame().getWorld().updateEnemyMovement(movementMessage, movementMessage.getPlayerId());


        } else {
            if (!movementMessage.isMoveIsValid()) {
                System.err.println("HIT");
                this.simulationMessageHandler.getGame().getWorld().updatePlayerMoveMent(movementMessage);
            }
        }

    }

    @Override
    public void announce() {

    }

    @Override
    public Class getHandledClass() {
        return MovementMessage.class;
    }
}