package de.lg.simulation;

import de.lg.network.Message;
import de.lg.network.messages.PlayerMessage;
import de.lg.world.Player;

/**
 * Created by Lars on 19.01.2016.
 */
public class SimulationPlayerMessageHandler implements SimulationMessageHandlerI {

    private SimulationMessageHandler simulationMessageHandler = null;


    public SimulationPlayerMessageHandler(SimulationMessageHandler simulationMessageHandler) {
        this.simulationMessageHandler = simulationMessageHandler;
    }

    @Override
    public Class getHandledClass() {
        return PlayerMessage.class;
    }

    @Override
    public void announce() {

    }

    @Override
    public void handleMessage(Message message) {
        PlayerMessage playerMessage = (PlayerMessage) message;
        // check if we are the player for oinit action or others to spawn
        System.out.println(this.simulationMessageHandler.getGame().authenticationToken);
        if (playerMessage.getAuthenticationToken().equals(this.simulationMessageHandler.getGame().authenticationToken)) {
            if(!playerMessage.getPlayerIdentifier().equals(this.simulationMessageHandler.getGame().authenticationToken)){
//                System.err.println("handle enemy player message");
//                Player player = new Player(playerMessage.getPositionX(), playerMessage.getPositionY());
//                this.simulationMessageHandler.getGame().getWorld().addEnemyPlayer(player, playerMessage.getAuthenticationToken());
            }else{
                Player player = new Player(playerMessage.getPositionX(), playerMessage.getPositionY());
                this.simulationMessageHandler.getGame().getWorld().setPlayer(player);
                this.simulationMessageHandler.getGame().isReady = true;
                System.out.println("player auth and online" + player.getCoordX() + " " + player.getCoordY());
            }
                   } else {

        }



    }
}
