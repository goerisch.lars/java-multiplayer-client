package de.lg.network.messages;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

/**
 * Created by Lars on 18.01.2016.
 */
class NoHeaderObjectOutputStream extends ObjectOutputStream {

    public NoHeaderObjectOutputStream(OutputStream os) throws IOException {
        super(os);
    }

    protected void writeStreamHeader() {}
}