package de.lg.network.messages;

import de.lg.network.Message;

import java.io.Serializable;

/**
 * Created by Lars on 22.01.2016.
 *
 * dummy Message to clear out Buffer
 */
public class DummyBufferMessage extends Message implements Serializable {
}
