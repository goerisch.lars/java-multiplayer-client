package de.lg.network.messages;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;

/**
 * Created by Lars on 18.01.2016.
 */
class NoHeaderObjectInputStream extends ObjectInputStream {

    public NoHeaderObjectInputStream(InputStream in) throws IOException {
        super(in);
    }

    @Override
    protected void readStreamHeader() throws IOException, StreamCorruptedException {

    }
}