package de.lg.network.messages;

import de.lg.network.Message;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Lars on 18.01.2016.
 */
public class MovementMessage extends Message implements Serializable {

    private String playerId = null;

    public boolean moveIsValid = false;

    public float moveToX = 0.0f;

    public float moveToY = 0.0f;
    private boolean KI = false;

    private String kiId = UUID.randomUUID().toString();

    public boolean isKI() {
        return KI;
    }

    public String getKiId() {
        return kiId;
    }

    public MovementMessage setKiId(String kiId) {
        this.kiId = kiId;
        return this;
    }

    public String getPlayerId() {
        return playerId;
    }

    public MovementMessage setPlayerId(String playerId) {
        this.playerId = playerId;
        return this;
    }

    public boolean isMoveIsValid() {
        return moveIsValid;
    }

    public MovementMessage setMoveIsValid(boolean moveIsValid) {
        this.moveIsValid = moveIsValid;
        return this;
    }

    public float getMoveToX() {
        return moveToX;
    }

    public MovementMessage setMoveToX(float moveToX) {
        this.moveToX = moveToX;
        return this;
    }

    public float getMoveToY() {
        return moveToY;
    }

    public MovementMessage setMoveToY(float moveToY) {
        this.moveToY = moveToY;
        return this;
    }

    public void setKI(boolean KI) {
        this.KI = KI;
    }
}
