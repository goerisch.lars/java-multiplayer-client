package de.lg.network;

import java.io.Serializable;
import java.util.UUID;

/**
 * after valid authentication the client get a new authToken to use in future communication
 * all messages from the client can auth them on its connectionClass
 * all messages must use a valid authToken
 * the auth token is only used between the server and the specific client
 *
 * Created by Lars on 20.01.2016.
 */
public abstract class AuthenticationToken implements Serializable {


    private String authenticationToken = null;

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public AuthenticationToken setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
        return this;
    }

    public static String generateToken(){
        return UUID.randomUUID().toString();
    }
}
