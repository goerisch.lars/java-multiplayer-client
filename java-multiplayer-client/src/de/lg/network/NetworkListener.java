package de.lg.network;


import de.lg.client.SynchronizedQueue;
import de.lg.network.messages.AuthenticationMessage;
import de.lg.network.messages.MovementMessage;
import de.lg.network.messages.ObjectSerialization;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by Lars on 30.12.2015.
 * <p>
 * listening for new connections from clients or servers and handle them to the correct process
 */
public class NetworkListener implements Runnable {

    NetworkMessageHandler networkMessageHandler = null;

    AsynchronousSocketChannel clientSocketChannel = null;

    ArrayList<ClientConnection> clientConnections = new ArrayList<>();

    ArrayList<AsynchronousSocketChannel> clientSocketChannels = new ArrayList<>();


    Connection connection = null;

    public final static int READ_MESSAGE_WAIT_TIME = 15;
    public final static int MESSAGE_INPUT_SIZE = 4096 * 5;

    // network -> simulation
    private SynchronizedQueue<Message> networkSimulationQueue;

    // simulation -> network
    private SynchronizedQueue<Message> simulationNetworkQueue;

    private CompletionHandler handler = null;

    final String ID = UUID.randomUUID().toString();

    public NetworkListener(SynchronizedQueue<Message> networkSimulationQueue, SynchronizedQueue<Message> simulationNetworkQueue) {
        this.networkSimulationQueue = networkSimulationQueue;
        this.simulationNetworkQueue = simulationNetworkQueue;
    }

    @Override
    public void run() {

        this.networkMessageHandler = new NetworkMessageHandler(this);

        System.out.println("NetworkListener running");
        this.openAsynchronousSocketChannel();


        while (true) {
            if (!this.simulationNetworkQueue.isEmpty()) {
                Collection<Message> messages = this.simulationNetworkQueue.getAll(true);
                for (Message message : messages) {
                    sendMessage(connection, message);
                }

            }

            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                //e.printStackTrace();
            }
        }
    }

    /**
     * send an message and add own auth token to message
     *
     * @param connection
     * @param message
     */
    private void sendMessage(Connection connection, Message message) {
        System.out.print(".");
        if (message instanceof MovementMessage) {
            if (((MovementMessage) message).isKI()) {
                message.setAuthenticationToken((this.connection.getAuthenticationToken()));
            } else {
                message.setAuthenticationToken(this.connection.getAuthenticationToken());
                this.writeObject(connection.getAsyncSocketChannel(), message);
            }

        } else {
            message.setAuthenticationToken(this.connection.getAuthenticationToken());
            this.writeObject(connection.getAsyncSocketChannel(), message);
        }
    }


    private void handleConnection(AsynchronousSocketChannel asyncSocketChannel) {

        ServerConnection serverConnection = new ServerConnection(asyncSocketChannel);
        this.connection = serverConnection;

        writeAuthenticationMessage(asyncSocketChannel);
        ;
        new ReadHandler(connection, this);

        // NetworkMessage networkMessage = NetworkMessageFactory.getNetworkMessage(object);
        //this.networkSimulationQueue.add(networkMessage);
    }

    /**
     * hell oserver i am marc and i want to connect
     *
     * @param asyncSocketChannel
     */
    private void writeAuthenticationMessage(AsynchronousSocketChannel asyncSocketChannel) {

        AuthenticationMessage message = new AuthenticationMessage(ID);

        this.writeObject(asyncSocketChannel, message);

    }

    public void handleMessageObject(Connection clientConnection, Object object) {
        if (object instanceof Message) {
            networkMessageHandler.proceedNetworkMessage(clientConnection, object);
        }
    }

    /**
     * open server socket channel with default threaded channel group
     * and add them to global class @var clientSocketChannel
     *
     * @return AsynchronousServerSocketChannel
     */
    private AsynchronousSocketChannel openAsynchronousSocketChannel() {
        System.out.println("open client channel");
        if (this.clientSocketChannel == null) {
            try {
                this.clientSocketChannel = AsynchronousSocketChannel.open();
                InetSocketAddress hostAddress = new InetSocketAddress("localhost", 9999);
                Future future = this.clientSocketChannel.connect(hostAddress);
                future.get();
                handleConnection(this.clientSocketChannel);
            } catch (IOException | InterruptedException | ExecutionException e) {
               // e.printStackTrace();
            }
        }
        System.out.println("client channel open");
        return this.clientSocketChannel;
    }


    /**
     *
     */
    private void closeNetwork() {
        try {
            this.clientSocketChannel.close();
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }


    private void writeObject(AsynchronousSocketChannel asyncSocketChannel, Object object) {
        try {
            ByteBuffer messageByteBuffer = ByteBuffer.wrap(ObjectSerialization.toByteArray(object));
            Future<Integer> futureWriteResult = asyncSocketChannel.write(messageByteBuffer);
            futureWriteResult.get(READ_MESSAGE_WAIT_TIME, TimeUnit.SECONDS);
            if (messageByteBuffer.hasRemaining()) {
                messageByteBuffer.compact();
            } else {
                messageByteBuffer.clear();
            }
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    public Object readObject(AsynchronousSocketChannel asyncSocketChannel) {
        try {
            ByteBuffer messageByteBuffer = ByteBuffer.allocate(MESSAGE_INPUT_SIZE);
            Future<Integer> futureReadResult = asyncSocketChannel.read(messageByteBuffer);
            futureReadResult.get(READ_MESSAGE_WAIT_TIME, TimeUnit.SECONDS);
            String s = new String(messageByteBuffer.array());
            System.out.println("new message");
            return s;
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void writeObject(Connection connection, Object object) {
        try {
            ByteBuffer messageByteBuffer = ByteBuffer.wrap(ObjectSerialization.toByteArray(object));
            Future<Integer> futureWriteResult = connection.getAsyncSocketChannel().write(messageByteBuffer);
            futureWriteResult.get(READ_MESSAGE_WAIT_TIME, TimeUnit.SECONDS);
            if (messageByteBuffer.hasRemaining()) {
                messageByteBuffer.compact();
            } else {
                messageByteBuffer.clear();
            }
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    public void addMessageToSimulation(Message message) {
        this.networkSimulationQueue.add(message);
    }


}
