package de.lg.network;

import de.lg.network.messages.ObjectSerialization;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

public class ReadHandler implements CompletionHandler<Integer, ByteBuffer> {

    private final NetworkListener networkListener;
    Connection connection = null;

    private AsynchronousSocketChannel _socket;

    private ByteBuffer _buffer;

    public ReadHandler(Connection connection, NetworkListener networkListener) {
        this.connection = connection;
        this.networkListener = networkListener;

        _buffer = ByteBuffer.allocate(NetworkListener.MESSAGE_INPUT_SIZE);
        this._socket = this.connection.getAsyncSocketChannel();
        this._socket.read(_buffer, _buffer, this);
    }

    @Override
    public void completed(Integer result, ByteBuffer attachment) {

        attachment.flip();
        //System.out.println("Message received");

        Object object = ObjectSerialization.toObject(attachment.array());
        this.networkListener.handleMessageObject(connection, object);

        _buffer.clear();
        this._socket.read(_buffer, _buffer, this);
    }

    @Override
    public void failed(Throwable exc, ByteBuffer attachment) {
        System.out.println(exc.getMessage());
    }
}