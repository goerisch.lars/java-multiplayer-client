package de.lg.network.handler;

import de.lg.network.Connection;
import de.lg.network.Message;
import de.lg.network.NetworkMessageHandler;
import de.lg.network.messages.AuthenticationMessage;

/**
 * Created by Lars on 18.01.2016.
 */
public class AuthenticationMessageHandler implements MessageHandler {

    private NetworkMessageHandler networkMessageHandler = null;

    public AuthenticationMessageHandler(NetworkMessageHandler networkMessageHandler) {
        this.networkMessageHandler = networkMessageHandler;
    }

    @Override
    public void handleMessage(Connection connection, Message message) {
        AuthenticationMessage authenticationMessage = (AuthenticationMessage) message;
       // this.sendAuthenticationInformation(connection,authenticationMessage);
        this.networkMessageHandler.getNetworkListener().addMessageToSimulation(authenticationMessage);
        System.out.println(authenticationMessage.getMessageInfo());
        connection.setAuthenticationToken(authenticationMessage.getAuthenticationToken());



        System.out.println(authenticationMessage.getAuthenticationId());
    }

    @Override
    public void announce() {

    }

    @Override
    public Class getHandledClass() {
        return AuthenticationMessage.class;
    }

    /**
     * sends greeting to the client
     *
     * @param connection
     * @param authenticationMessage
     */
    private void sendAuthenticationInformation(Connection connection, AuthenticationMessage authenticationMessage){
        authenticationMessage.setMessageInfo("client connected, wait for instructions");
        this.networkMessageHandler.getNetworkListener().writeObject(connection, authenticationMessage);
    }

}
