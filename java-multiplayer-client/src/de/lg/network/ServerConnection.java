package de.lg.network;

import java.nio.channels.AsynchronousSocketChannel;

/**
 * Created by Lars on 18.01.2016.
 */
public class ServerConnection extends Connection {

    public ServerConnection(AsynchronousSocketChannel asynchronousSocketChannel) {
        super(asynchronousSocketChannel);
    }
}
